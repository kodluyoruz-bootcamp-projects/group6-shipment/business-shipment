package com.example.demo.service;

import com.example.demo.domain.BusinessShipment;
import com.example.demo.exception.BusinessAccountNotFoundException;
import com.example.demo.exception.BusinessShipmentNotCreatedException;
import com.example.demo.exception.BusinessShipmentNotFoundException;
import com.example.demo.repository.BusinessShipmentRepository;
import com.example.demo.service.BusinessShipmentService;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BusinessShipmentServiceTests {
    @Mock
    BusinessShipmentRepository businessShipmentRepository;
    @InjectMocks
    BusinessShipmentService businessShipmentService;

    @SneakyThrows
    @Test
    public void shouldThrowBusinessShipmentNotFoundExceptionWhenThereIsNoShipmentWithId() {
        //Arrange
        String trackingCode = "1";
        when(businessShipmentRepository.getBusinessShipmentByTrackingCode(trackingCode)).thenThrow(new BusinessShipmentNotFoundException("Business Shipment Cannot Found"));
        //Act
        Throwable throwable = catchThrowable(() -> businessShipmentService.getBusinessShipmentByTrackingCode(trackingCode));
        //Assert
        assertThat(throwable).isInstanceOf(BusinessShipmentNotFoundException.class).hasMessage("Business Shipment Cannot Found");
    }

    @SneakyThrows
    @Test
    public void shouldThrowBusinessAccountNotFoundExceptionWhenThereIsNoBusinessAccountWithId() {
        //Arrange
        String companyId = "1";
        when(businessShipmentRepository.getAllBusinessShipmentByAccountID(companyId)).thenThrow(new BusinessAccountNotFoundException("Business Account With Id Not Found"));
        //Act
        Throwable throwable = catchThrowable(() -> businessShipmentService.getAllBusinessShipmentByAccountID(companyId));
        //Assert
        assertThat(throwable).isInstanceOf(BusinessAccountNotFoundException.class).hasMessage("Business Account With Id Not Found");
    }

    @SneakyThrows
    @Test
    public void shouldThrowBusinessShipmentNotCreatedExceptionWhenBusinessShipmentIsNotValid() {
        //Arrange
        BusinessShipment businessShipment = new BusinessShipment();
        String id = "1";
        doThrow(BusinessShipmentNotCreatedException.class).when(businessShipmentRepository).insertBusinessShipment(any(String.class), any(BusinessShipment.class));
        //Act
        Throwable throwable = catchThrowable(() -> businessShipmentService.createBusinessShipment(id, businessShipment));
        //Assert
        assertThat(throwable).isInstanceOf(BusinessShipmentNotCreatedException.class);
    }
}
