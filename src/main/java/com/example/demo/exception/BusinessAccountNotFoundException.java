package com.example.demo.exception;

public class BusinessAccountNotFoundException extends Exception{
    public BusinessAccountNotFoundException(String message){
        super(message);
    }
}
