package com.example.demo.exception;

public class BusinessShipmentNotCreatedException extends Exception {
    public BusinessShipmentNotCreatedException(String message) {
        super(message);
    }
}
