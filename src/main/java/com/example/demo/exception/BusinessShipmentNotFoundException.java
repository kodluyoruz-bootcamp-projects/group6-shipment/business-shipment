package com.example.demo.exception;

public class BusinessShipmentNotFoundException extends Exception{
    public BusinessShipmentNotFoundException(String message){
        super(message);
    }
}
