package com.example.demo.service;

import com.example.demo.domain.BusinessShipment;
import com.example.demo.exception.BusinessAccountNotFoundException;
import com.example.demo.exception.BusinessShipmentNotCreatedException;
import com.example.demo.exception.BusinessShipmentNotFoundException;
import com.example.demo.repository.BusinessShipmentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessShipmentService {
    private final BusinessShipmentRepository businessShipmentRepository;

    public BusinessShipmentService(BusinessShipmentRepository businessShipmentRepository) {
        this.businessShipmentRepository = businessShipmentRepository;
    }

    public void createBusinessShipment(String id, BusinessShipment businessShipment) throws BusinessShipmentNotCreatedException {
        businessShipmentRepository.insertBusinessShipment(id, businessShipment);
    }

    public BusinessShipment getBusinessShipmentByTrackingCode(String trackingCode) throws BusinessShipmentNotFoundException {
        return businessShipmentRepository.getBusinessShipmentByTrackingCode(trackingCode);
    }

    public List<BusinessShipment> getAllBusinessShipmentByAccountID(String id) throws BusinessAccountNotFoundException {
        return businessShipmentRepository.getAllBusinessShipmentByAccountID(id);
    }
}
