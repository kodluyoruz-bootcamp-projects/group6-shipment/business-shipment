package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Address {
    private String city;
    private String distinct;
    private String street;
    private String no;

    public Address() {
    }

    public Address(String city, String distinct, String street, String no) {
        this.city = city;
        this.distinct = distinct;
        this.street = street;
        this.no = no;
    }
}
