package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class BusinessShipment {

    private Person receiver;
    private ShipmentPackage shipmentPackage;
    private String shipmentType;
    private String payer;
    private String status;
    private String businessId;
    private String trackingCode;

    public BusinessShipment() {
        this.trackingCode = UUID.randomUUID().toString();
    }

}
