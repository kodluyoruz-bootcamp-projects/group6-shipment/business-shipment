package com.example.demo.repository;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.query.QueryResult;
import com.example.demo.domain.BusinessShipment;
import com.example.demo.exception.BusinessAccountNotFoundException;
import com.example.demo.exception.BusinessShipmentNotCreatedException;
import com.example.demo.exception.BusinessShipmentNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BusinessShipmentRepository {

    private final Cluster couchbaseCluster;
    private final Collection businessShipmentCollection;

    public BusinessShipmentRepository(Cluster couchbaseCluster, Collection businessShipmentCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.businessShipmentCollection = businessShipmentCollection;
    }

    public void insertBusinessShipment(String id, BusinessShipment businessShipment) throws BusinessShipmentNotCreatedException {
        if (isValidBusinessShipment(businessShipment))
            throw new BusinessShipmentNotCreatedException("Business Shipment Cannot Created");
        businessShipment.setBusinessId(id);
        businessShipmentCollection.insert(businessShipment.getTrackingCode(), businessShipment);
    }

    private boolean isValidBusinessShipment(BusinessShipment businessShipment) {
        return businessShipment.getShipmentPackage() == null && businessShipment.getPayer() == null && businessShipment.getReceiver() == null && businessShipment.getShipmentType() == null;
    }

    public BusinessShipment getBusinessShipmentByTrackingCode(String trackingCode) throws BusinessShipmentNotFoundException {
        try {
            return businessShipmentCollection.get(trackingCode).contentAs(BusinessShipment.class);
        } catch (Exception e) {
            throw new BusinessShipmentNotFoundException("Business Shipment Cannot Found");
        }
    }

    public List<BusinessShipment> getAllBusinessShipmentByAccountID(String id) throws BusinessAccountNotFoundException {
        try {
            businessShipmentCollection.get(id);
            String statement = String.format("select group6.* from group6 where businessId='%s'", id);
            QueryResult result = couchbaseCluster.query(statement);
            return result.rowsAs(BusinessShipment.class);
        } catch (Exception e) {
            throw new BusinessAccountNotFoundException("Business Account With Id Not Found");
        }
    }
}
